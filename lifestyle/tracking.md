# Tracking

## Caffeine

| Product                                                                                                                         | Caffeine *(mg)* |
|---------------------------------------------------------------------------------------------------------------------------------|:---------------:|
| [Stumptown Coldbrew (16oz)](http://www.caffeineinformer.com/caffeine-content/stumptown-coffee-cold-brew)                        |     425 mg      |
| [Coffee, brewed, 16oz](http://www.mayoclinic.org/healthy-lifestyle/nutrition-and-healthy-eating/in-depth/caffeine/art-20049372) |     300 mg      |
| [OiOcha Green Tea](http://www.caffeineinformer.com/caffeine-content/oi-ocha-green-tea)                                          |      60 mg      |
