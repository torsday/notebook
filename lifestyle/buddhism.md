# Buddhism

> “No one in the world ever gets what they want and that is beautiful.”

> ~ Cline, Ernest. Ready Player One.

## 4 Noble Truths

1.  Suffering is a part of life.
1.  There is a cause to suffering.
1.  There is an end to suffering.
1.  The end to suffering is contained in the Eightfold Path

## Eightfold Path

### Right:

-   View
-   Intention
-   Speech
-   Action
-   Livelihood
-   Effort
-   Mindfulness
-   Concentration
