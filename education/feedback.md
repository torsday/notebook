# Feedback

---

## Big Ideas

-   See them first
-   Focus on the bright spots
-   Share the same language: SBI
-   Work the gap
-   Shrink the Change

---

## SBI *(Situation, Behavior, Impact)*

### Situation

-   Setting and circumstances
-   The *"Where"* or *"When"*

### Behavior

-   Observable actions
-   Provide 1-3 representative examples.

### Impact

-   Emotion
-   Deadlines
-   Quality
-   Morale
-   Business Goals
-   Career Goals

---

## To Chew On

-   What behaviors are you reinforcing and modeling?
-   How will you model the behavior your are seeking for others?
-   We get the behavior we reinforce, not the behavior we want.

---

Traits vs. Behaviors

---

## My Experience

-   Vulnerability. When giving feedback, approach from a place of vulnerability
-   Thank them when you receive it
-   Don't respond to feedback with feedback
-   If you can't find the flipside positive of a constructive criticism, pause before going forward, there may be a better place for you to come from within yourself before going to the other person.
