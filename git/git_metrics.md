# Git Metrics

---

## Get metrics on diff

### `gd --stat 7455ecbbad8 HEAD`

```sh
% gd --stat 7455ecbbad8 HEAD
 docroot/application/models/credit/CreditAlertShardedTbl.php                    | 80 ++++++++++++++++++++++++++++++
 docroot/application/models/credit/CreditAlertTbl.php                           | 71 +++++++++++++++++++++++++++
 .../application/modules/mobile/controllers/NotificationRoutingController.php   | 70 ++++++++++++++++-----------
 .../util/emailCampaign/campaign/ck/CreditAlertV2_MessageCreatorView.php        |  2 +-
 .../util/gearman/job/type/v2/creditAlertNotification/CreditAlertProcessor.php  |  2 +-
 .../gearman/job/workload/emailCampaign/bulk/ck/CreditAlertV2_SingleAlert.php   |  4 +-
 .../Service/CreditAlert/Application/Service/AggregateCreditAlertService.php    | 25 ++++++++--
 20 files changed, 517 insertions(+), 271 deletions(-)
 ```

Get last line, send to clipboard

```sh
gd --stat 7455ecbbad8 HEAD | tail -n1 | pbcopy
gd --stat HEAD^ HEAD | tail -n1 | pbcopy
```

copies `20 files changed, 517 insertions(+), 271 deletions(-)` to the clipboard
