# Glossary

## APNs

Apple Push Notification service.

## Behavior-Driven Development

A software development methodology in which an application is specified and designed by describing how its behavior should appear to an outside observer.

## Binary Protocol

A protocol which is intended or expected to be read by a machine rather than a human being, as opposed to a plain text protocol such as IRC, SMTP, or HTTP. Binary protocols have the advantage of terseness, which translates into speed of transmission and interpretation.

## Blocks

In Ruby, blocks are closures (anonymous functions) that can be passed to many methods.

## Brevity

Concise and exact use of words in writing or speech.

## Callbacks

Functions that are executed asynchronously.

## Comparison Sort

A type of sorting algorithm that only reads the list elements through a single abstract comparison operation (often a "less than or equal to" operator or a three-way comparison) that determines which of two elements should occur first in the final sorted list.

## Composer

An application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries.

## Control Flow

The order in which individual statements, instructions or function calls of an imperative program are executed or evaluated. The emphasis on explicit control flow distinguishes an imperative programming language from a declarative programming language.

## CORS

Cross-Origin Resource Sharing.

## CRUD

Create. Read. Update. Destroy.

## Curry

See: Currying.

## Currying

The process of transforming a function that takes multiple arguments into a function that takes just a single argument and returns another function if any arguments are still needed.

## Delta Compression

See: Delta Encoding.

## Delta Encoding

A way of storing or transmitting data in the form of differences (deltas) between sequential data rather than complete files; more generally this is known as data differencing.

## DIT

Directory Information Tree.

## Divide & conquer algorithm

An algorithm design paradigm based on multi-branched recursion. A divide and conquer algorithm works by recursively breaking down a problem into two or more sub-problems of the same (or related) type (divide), until these become simple enough to be solved directly (conquer). The solutions to the sub-problems are then combined to give a solution to the original problem.

## Docker

Docker, a tool for deploying and running applications. Docker provides a way to run an application securely isolated in a container in a way that is platform agnostic. <https://docs.docker.com>

## Domain-Specific Language

A computer language specialized to a particular application domain.

## DRY

Don’t Repeat Yourself, a software design principle. Explained in Pragmatic Programmers, it means that “Every piece of knowledge must have a single, unambiguous, authoritative representation within a system.”

## DSE

DSA Specific Entry.

## Duck Typing

A way to write computer code that doesn't demand that you know what types you are writing about. If it does what you thought it would do, it is good enough. The reason this is called "duck typing" is the duck test: "When I see a bird that walks like a duck and swims like a duck and quacks like a duck, I call that bird a duck."

## Functional Tests

Functional tests check a particular feature for correctness by comparing the results for a given input against the specification. Functional tests don't concern themselves with intermediate results or side-effects, just the result (they don't care that after doing x, object y has state z). They are written to test part of the specification such as, "calling function Square(x) with the argument of 2 returns 4".

## General-Purpose Language

A computer language that is broadly applicable across application domains, and lacks specialized features for a particular domain.

## GNU Make

A tool which controls the generation of executables and other non-source files of a program from the program's source files.

## Headless

A headless browser is a web browser without a graphical user interface.

## HMAC

Hash-based message authentication code.

## Interface

Specifies what methods a class must implement (an interface). An interface is slightly more abstract than an abstract class, because it does not imply an 'is a' relationship with classes that inherit it.

## Kitematic

The Docker GUI, runs on Mac OS X and Windows operating systems. <https://docs.docker.com/kitematic>

## Lambda

A closure. More or less identical to PROCS, with a key difference: when a lambda returns, it passes control back to the calling method; when a proc returns, it does so immediately, without finishing the calling method.

## Law of Demeter

A given object should assume as little as possible about the structure or properties of anything else (including its subcomponents), in accordance with the principle of "information hiding".

## Lazy Loading

A design pattern commonly used in computer programming to defer initialization of an object until the point at which it is needed. It can contribute to efficiency in the program's operation if properly and appropriately used. The opposite of lazy loading is eager loading.

## LDAP

A directory service protocol that runs on a layer above the TCP/IP stack. It provides a mechanism used to connect to, search, and modify Internet directories. The LDAP directory service is based on a client-server model.

## LDIF

A standard plain text data interchange format for representing LDAP directory CRUD requests.

## Ncurses

A programming library providing an API that allows the programmer to write text-based user interfaces in a terminal-independent manner. It is a toolkit for developing "GUI-like" application software that runs under a terminal emulator.

## OID

Object IDentifier.

## OKRs

Objectives and Key Results (OKRs) are a management methodology that connects work to a strategic plan.

## Ontology

The philosophical study of the nature of being, becoming, existence, or reality, as well as the basic categories of being and their relations. It deals with questions about what things exist or can be said to exist, and how such entities can be grouped according to similarities and differences.

## OSPNS

Operating System Push Notification Service.

## Polymorphism

The provision of a single interface to entities of different types.

## Postgres

See: PostgreSQL.

## PostgreSQL

An object-relational database management system (ORDBMS) with an emphasis on extensibility and standards-compliance.

## Proc

A closure or a "saved block”. Kind of like a method, but it can be used like a block and passed to methods that take blocks as parameters. & is used to convert a proc into a block.

## Protected

Accessible within the class itself and subclasses.

## Protocol

A defined language machines talk in. Doesn't matter how their brains created or listen to the thoughts, just that the language is agreed upon.

## RDN

Relative Distinguished Name.

## Redux

Redux is a predictable state container for JavaScript apps.

## Reflection

The ability of a computer program to examine (see type introspection) and modify its own structure and behavior (specifically the values, meta-data, properties and functions) at runtime.

## Remote Procedure Call

In distributed computing, a remote procedure call (RPC) is when a computer program causes a procedure (subroutine) to execute in a different address space (commonly on another computer on a shared network), which is coded as if it were a normal (local) procedure call, without the programmer explicitly coding the details for the remote interaction. That is, the programmer writes essentially the same code whether the subroutine is local to the executing program, or remote.[1] This is a form of client–server interaction (caller is client, executor is server), typically implemented via a request–response message-passing system. In the object-oriented programming paradigm, RPC calls are represented by remote method invocation (RMI). The RPC model implies a level of location transparency, namely that calling procedures is largely the same whether it is local or remote, but usually they are not identical, so local calls can be distinguished from remote calls. Remote calls are usually orders of magnitude slower and less reliable than local calls, so distinguishing them is important.

## Repository

A Repository mediates between the domain and data mapping layers, acting like an in-memory domain object collection

## RSA

A cryptosystem for public-key encryption.

## SAML

Security Assertion Markup Language Tokens.

## SEIT

Software Engineer in Test

## Sequence Diagram

An interaction diagram that shows how processes operate with one another and in what order.

## Service

A Service in Domain Driven Design is simply a stateless object that performs an action.

## Sorting Algorithm

An algorithm that puts elements of a list in a certain order.

## SQLite

 A software library that implements a self-contained, serverless, zero-configuration, transactional SQL database engine.

## Stable Sort

The implementation preserves the input order of equal elements in the sorted output.

## Trait

A set of methods that are not inherited, but composed into classes.

## Traits

*see Trait*

## Type introspection

The ability of a program to examine the type or properties of an object at runtime.

## UUID

A universally unique identifier (UUID) is an identifier standard used in software construction. A UUID is simply a 128-bit value.

## WebSockets

An advanced technology that makes it possible to open an interactive communication session between the user's browser and a server. With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

## X.500

A complex enterprise directory system.

## XML

Extensible Markup Language.

## XMPP

Extensible Messaging & Presence Protocol.
