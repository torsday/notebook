# Agile

## Meetings

### Sprint Planning

-   **Purpose:**

### Backlog Grooming

The intent is to ensure that the backlog remains populated with items that are relevant, detailed and estimated to a degree appropriate with their priority, and in keeping with current understanding of the project or product and its objectives.

-   removing user stories that no longer appear relevant
-   creating new user stories in response to newly discovered needs
-   re-assessing the relative priority of stories
-   assigning estimates to stories which have yet to receive one
-   correcting estimates in light of newly discovered information
-   splitting user stories which are high priority but too coarse grained to fit in an upcoming iteration

### Sprint Review

-   **Purpose:**
