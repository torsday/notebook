# GraphQL

---

> a data query language developed internally by Facebook in 2012 before being publicly released in 2015.  It provides an alternative to REST and ad-hoc webservice architectures.. It allows clients to define the structure of the data required, and exactly the same structure of the data is returned from the server. It is a strongly typed runtime which allows clients to dictate what data is needed. This avoids both the problems of over-fetching as well as under-fetching of data. GraphQL has been implemented in multiple languages, including JavaScript, Python, Ruby, Java, C#, Scala, Go, Elixir, Erlang, PHP, and Clojure.
> * Wikipedia

![Diagram of Rest vs. GraphQL](https://cdn-images-1.medium.com/max/1600/1*f_XvFD7FvliMM74WHJ0vRQ.png)

---

## NOTES

- One schema for the client
- Common Types
- Document Node (different from schema)
- Runtime isolation

---

## RESOURCES

- [Official Site](http://graphql.org)
- [GraphQL Hub](https://www.graphqlhub.com/)
