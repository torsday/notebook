# SOLID Principles

A mnemonic acronym for principles of OOP.

-   **S**ingle Responsibility
-   **O**pen/closed
-   **L**iskov Substitution
-   **I**nterface Segregation
-   **D**ependency Inversion

## Single Responsibility Principle

> A class should have only a single responsibility.

## Open/closed Principle

> Software entities should be open for extension, but closed for modification.

## Liskov Substitution Principle

> Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program.

> *See also [design by contract](https://en.wikipedia.org/wiki/Design_by_contract).*

## Interface Segregation Principle

> Many client-specific interfaces are better than one general-purpose interface.

## Dependency Inversion Principle

> Depend upon Abstractions, not on concretions.

## References

-   <https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)>
