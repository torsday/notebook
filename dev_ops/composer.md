# Composer

An application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries.

```sh
composer install

composer update cpt/rch-php

composer require "cpt/rch-php=0.73.*"
```
