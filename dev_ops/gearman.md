# Gearman

*From: [GearmanHQ](http://gearmanhq.com/help/getting-started)*

> A generic application framework for doing work in the background, spanning different machines, networks, and workers, each suited to preforming a specific job or set of jobs. Gearman itself acts as the central nervous system: jobs can come in from a variety of source, and workers can be on several machines and running in several different langauges.

![Gearman Flowchart](https://upload.wikimedia.org/wikipedia/en/c/c5/Gearman_Stack.png)

------------------------------------------------------------------------------------------------------------------------

## TESTING

```sh
# stop workers
sudo supervisorctl stop cs-gearman-workers:* # stop workers

# start workers
debugworker # start debug workers
```
