# Jenkins

An open source application written in Java.

It is one of the most popular continuous integration tools used to build and test different kinds of projects.

Used to automate development workflow, commonly:

-   Building projects
-   Running tests to detect bugs and other issues as soon as they are introduced
-   Static code analysis
-   Deployment

## References

-   [Github: jenkinsci/jenkins](https://github.com/jenkinsci/jenkins)
-   [Jenkins-CI.org/](https://jenkins-ci.org)
