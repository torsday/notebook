# Summary

-   [Introduction](README.md)

-   [Learning Log](learning_log.md)

-   [Algorithms](algorithms/README.md)

    -   [Sorting Algorithms](algorithms/sorting.md)

        -   [Heapsort](algorithms/heapsort.md)
        -   [Merge Sort](algorithms/merge_sort.md)
        -   [Quicksort](algorithms/quicksort.md)
        -   [Selection Sort]()

    -   [Search Algorithms](algorithms/search.md)

        -   [Binary Search](algorithms/binary_search.md)

    -   [Other](algorithms/other.md)

        -   [Data Compression Algorithms]()
        -   [Dijkstra’s Algorithm](algorithms/dijkstras_algorithm.md)
        -   [Fourier Transform & Fast Fourier Transform]()
        -   [HMAC](./algorithms/hmac.md)
        -   [Integer Factorization]()
        -   [Link Analysis]()
        -   [Proportional Integral Derivative Algorithm]()
        -   [Random Number Generation]()
        -   [RSA](./algorithms/rsa.md)
        -   [Secure Hash]()

-   [Articles](articles/README.md)

    -   [Squash](articles/squash.md)
    -   [The Right PHP LDAP Library](articles/search_for_php_ldap_library.md)

-   [Debugging](debugging/README.md)

    -   [Pry]()
    -   [XDebug]()

-   [Design](./design/README.md)

    -   [Design Patterns](./design/./design_patterns.md)

        -   [Creational](./design/creational.md)

            -   [Abstract Factory](./design/abstract_factory.md)
            -   [Factory Method](./design/factory_method.md)
            -   [Singleton](./design/singleton.md)

        -   [Structural](./design/structural.md)

            -   [Adapter](/./design/adapter.md)
            -   [Bridge](/./design/bridge.md)
            -   [Composite](/./design/composite.md)
            -   [Decorator](/./design/decorator.md)
            -   [Flyweight](/./design/flyweight.md)
            -   [Proxy](/./design/proxy.md)

        -   [Behavioral](./design/behavioral.md)

            -   [Chain of Responsibility](./design/chain_of_responsibility.md)
            -   [Command](./design/command.md)
            -   [Iterator](./design/iterator.md)
            -   [Mediator](./design/mediator.md)
            -   [Memento](./design/memento.md)
            -   [Observer](./design/observer.md)
            -   [State](./design/state.md)
            -   [Strategy](./design/strategy.md)
            -   [Template](./design/template.md)

    -   [Architecture](./design/architecture.md)

        -   [Domain Driven Design](./design/ddd.md)
        -   [Hexagonal](./design/hexagonal.md)
        -   [Onion](./design/onion.md)
        -   [REST](./design/rest.md)

    -   [Other](./design/other.md)

        -   [Active Record Pattern](./design/active_record_pattern.md)
        -   [CQRS](./design/cqrs.md)
        -   [Lazy Loading](./design/lazy_loading.md)
        -   [MVC](./design/mvc.md)
        -   [Object-Oriented Programming](./design/oop.md)
        -   [Pub-sub Pattern](./design/pub_sub.md)
        -   [SOLID Principles](./design/solid.md)
        -   [Specification Pattern](./design/specification_pattern.md)

-   [DevOps](./dev_ops/README.md)

    -   [Artifactory]()
    -   [Babel]()
    -   [Build & Release]()
    -   [Composer](./dev_ops/composer.md)
    -   [Continuous Integration](./dev_ops/ci.md)
    -   [Docker](./dev_ops/docker.md)
    -   [Gearman](./dev_ops/gearman.md)
    -   [Grunt]()
    -   [Gulp](./dev_ops/gulp.md)
    -   [Jenkins]()
    -   [JWT](./dev_ops/jwt.md)
    -   [Nginx]()
    -   [NPM](./dev_ops/npm.md)
    -   [NVM](./dev_ops/nvm.md)
    -   [Okta]()
    -   [Phing](./dev_ops/phing.md)
    -   [SAML]()
    -   [Splunk]()
    -   [Travis CI](./dev_ops/travis.md)
    -   [Vagrant]()

-   [Education](./education/README.md)

    -   [Courses](./education/courses.md)

        -   [Harvard CS50](./education/harvard_cs50.md)

    -   [OKRs](./education/okrs.md)

    -   [Other](./education/other.md)

        -   [Exercism](./education/exercism.md)
        -   [Feedback](./education/feedback.md)

-   [Internet](./internet/README.md)

    -   [HTTP](./internet/http.md)
    -   [LDAP](./internet/ldap.md)
    -   [OAuth](./internet/oauth.md)
    -   [Push Notifications](./internet/push_notifications.md)
    -   [TCP/IP]()
    -   [TLS/SSL](./internet/tls_ssl.md)
    -   [XMPP](./internet/xmpp.md)

-   [Languages, Libraries, Frameworks, et. al.](./languages/README.md)

    -   [C](./languages/c.md)
    -   [CoffeeScript](./languages/coffeescript.md)
    -   [Javascript](./languages/javascript.md)
    -   [Laravel]()
    -   [Lo-Dash]()
    -   [Markdown](./languages/markdown.md)
    -   [Node]()
    -   [Objective-C](./languages/objective-c.md)
    -   [PHP the Right Way](./languages/php-the-right-way.md)
    -   [PHP](./languages/php.md)
    -   [Rails](./languages/rails.md)
    -   [React](./languages/react.md)
    -   [Redux](./languages/redux.md)
    -   [Ruby](./languages/ruby.md)
    -   [Scala](./languages/scala.md)
    -   [Swift](./languages/swift.md)
    -   [Symfony]()
    -   [Zend Frameworks]()

-   [Lifestyle](./lifestyle/README.md)

    -   [Books](./lifestyle/books.md)
    -   [Buddhism](./lifestyle/buddhism.md)
    -   [Campsite Wishlist](./lifestyle/campsite_wishlist.md)
    -   [Tracking](./lifestyle/tracking.md)
    -   [Zen](./lifestyle/zen.md)

-   [Linux](./linux/README.md)

    -   [Tree](./linux/tree.md)
    -   [Air Crack]()
    -   [Arp Scan]()
    -   [Cron](./linux/cron.md)
    -   [du](./linux/du.md)
    -   [find]()
    -   [grep](./linux/grep.md)
    -   [rsync](./linux/rsync.md)

-   [Persistence](./persistence/README.md)

    -   [CouchDB]()
    -   [Hadoop]()
    -   [HBase]()
    -   [MemCached]()
    -   [MongoDB]()
    -   [MySQL]()
    -   [Neo4J]()
    -   [Postgres](./persistence/postgres.md)
    -   [Redis]()
    -   [Riak]()
    -   [SQL](./persistence/sql.md)
    -   [SQLite](./persistence/sqlite.md)

-   [Security](./security/README.md)

    -   [Common Web Flaws](./security/common_web_./security_flaws.md)
    -   [Encryption](./security/encryption.md)
    -   [SSH](./security/ssh.md)

-   [Testing](./testing/README.md)

    -   [Cucumber]()
    -   [Jasmine]()
    -   [Jest]()
    -   [Mocha]()
    -   [PHPUnit]()
    -   [Rspec](./testing/rspec.md)
    -   [Selenium](./testing/selenium.md)

-   [Tools](./tools/README.md)

    -   [Atom]()
    -   [Git](./tools/git.md)
    -   [Homebrew](./tools/homebrew.md)
    -   [Linting]()
    -   [Linux](./tools/linux.md)
    -   [PSR Fixer](./tools/psr-fixer.md)
    -   [UML](./tools/uml.md)
    -   [Vim](./tools/vim.md)

-   [References](references.md)
