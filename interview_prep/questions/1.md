## Question

If you have a binary tree like so:

```
      4
  7      12
43 8   23  45
             7
```

Return the top two vals, max first.

```ruby
[4, null]
[12, 7]
[45, 43]
[7, null]
```

Given

```ruby
# Node makeup
class Node
  @value      = _value
  @left_node  = _node_l
  @right_node = _node_r
end

class Answer
  # method to build off of
  def find(root_node)
    # put code here

    # Should return:
    # [4, null]
    # [12, 7]
    # [45, 43]
    # [7, null]
  end
end
```

---

For my own use, creation of the test data. Likely wouldn't have to do this in an interview.

```ruby
top_node = Node.new(4,)
```

## Solution(s)

---

```ruby
# Node makeup
class Node
  attr_accessor :value, :left_node, :right_node

  def initialize(_value, _node_l, _node_r)
    @value      = _value
    @left_node  = _node_l
    @right_node = _node_r
  end
end

class Answer
  attr_accessor :answer
  def initialize
    @answer = {}
  end
  # method to build off of
  def find(root_node)

    generation = 0

    update_answer_with_node(generation, root_node.left_node, root_node.right_node)

    # put code here

    # Should return:
    # [4, null]
    # [12, 7]
    # [45, 43]
    # [7, null]

    answer
  end

  def update_answer_with_node(generation, left_node, right_node)
    if generation === 0
      answer[generation] = [left_node.value, right_node.value].sort.reverse
    elsif condition
    end

    
  end
end

```

---
