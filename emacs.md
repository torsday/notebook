# Emacs

---

## Installation

```sh
# https://www.reddit.com/r/emacs/comments/4pocdd/advice_on_setting_up_emacs_on_os_x/
# Outdated?
$ brew install emacs --with-cocoa --srgb
$ brew linkapps emacs
$ brew services start emacs
```

```sh
$ brew install emacs -nw
```

```sh
$ brew install emacs -ns
```

## COMMANDS

### Quick sheet

| Key(s) | Description                             |
|:------:|:----------------------------------------|
| `C-g`  | quit running or partially typed command |

### Help

Emacs has extensive online help, most of which is available via the help key, `C-h`. `C-h` is a prefix key. Type `C-h` twice to see a list of subcommands; type it three times to get a window describing all these commands (a SPC will scroll this window). Some of the most useful help commands are:

|  Key(s)   | Description                                                                                                    |
|:---------:|:---------------------------------------------------------------------------------------------------------------|
| `C-h` `a` | `command-apropos`. Prompts for a keyword and then lists all the commands with that keyword in their long name. |
| `C-h` `k` | `describe-key`. Prompts for a keystroke and describes the command bound to that key, if any.                   |
| `C-h` `i` | info. Enters the Info hypertext documentation reader.                                                          |
| `C-h` `m` | `describe-mode`. Describes the current major mode and its particular key bindings.                             |
| `C-h` `p` | `finder-by-keyword`. Runs an interactive subject-oriented browser of Emacs packages.                           |
| `C-h` `t` | `help-with-tutorial`. Run the Emacs tutorial. This is very helpful for beginners.                              |

### Legend

| Key(s)  | Description                                                                                                              |
|:-------:|:-------------------------------------------------------------------------------------------------------------------------|
|  `C-x`  | For any x, the character Control-x.                                                                                      |
|  `M-x`  | For any x, the character Meta-x (see below for more details on Meta characters). Using an actual `x` gives you a lookup. |
| `C-M-x` | For any x, the character Control-Meta-x.                                                                                 |
|  `RET`  | The return key (C-m actually).                                                                                           |
|  `SPC`  | The space bar.                                                                                                           |
|  `ESC`  | The escape key, or, equivalently, `C-[`                                                                                  |

### Standard Prefix Commands

| Key(s) | Description                                                                                                                                                                      |
|:------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `C-c`  | Used for commands that are specific to particular modes, so they are free to be used for different commands depending on context. These are the most variable of Emacs commands. |
| `C-h`  | Used for Help commands.                                                                                                                                                          |
| `C-x`  | This prefix is used mostly for commands that manipulate files, buffers and windows.                                                                                              |

### Basics

To exit Emacs, use the command `C-x` `C-c` (which is bound to `save-buffers-kill-emacs`). It will offer to save all your buffers and then exit.

You can also suspend Emacs (in the Unix sense of stopping it and putting it in the background) with `C-x` C-z (which is bound to suspend-emacs). How you restart it is up to your shell, but is probably based on the fg command.

### Navigation

| Key(s) | Description |
|:------:|:------------|
|  C-B   | Left        |
|  C-N   | Down        |
|  C-P   | Up          |
|  C-F   | Right       |

### Manipulate Files

|   Key(s)    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|:-----------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `C-x` `C-f` | `find-file`. This is the main command used to read a file into a buffer for editing. It's actually rather subtle. When you execute this command, it prompts you for the name of the file (with completion). Then it checks to see if you're already editing that file in some buffer; if you are, it simply switches to that buffer and doesn't actually read in the file from disk again. If you're not, a new buffer is created, named for the file, and initialized with a copy of the file. In either case the current window is switched to view this buffer. |
| `C-x` `C-s` | `save-buffer`. This is the main command used to save a file, or, more accurately, to write a copy of the current buffer out to the disk, overwriting the buffer's file, and handling backup versions.                                                                                                                                                                                                                                                                                                                                                              |
|  `C-x` `s`  | `save-some-buffers`. Allows you to save all your buffers that are visiting files, querying you for each one and offering several options for each (save it, don't save it, peek at it first then maybe save it, etc).                                                                                                                                                                                                                                                                                                                                              |

### Manipulate Buffers

|   Key(s)    | Description                                                                                                                                                                                                                                                                                               |
|:-----------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  `C-x` `b`  | `switch-to-buffer`. Prompts for a buffer name and switches the buffer of the current window to that buffer. Doesn't change your window configuration. This command will also create a new empty buffer if you type a new name; this new buffer will not be visiting any file, no matter what you name it. |
| `C-x` `C-b` | `list-buffers`. Pops up a new window which lists all your buffers, giving for each the name, modified or not, size in bytes, major mode and the file the buffer is visiting.                                                                                                                              |
|  `C-x` `k`  | `kill-buffer`. Prompts for a buffer name and removes the entire data structure for that buffer from Emacs. If the buffer is modified you'll be given an opportunity to save it. Note that this in no way removes or deletes the associated file, if any.                                                  |
| `C-x` `C-q` | `vc-toggle-read-only`. Make a buffer read-only (so that attempts to modify it are treated as errors), or make it read-write if it was read-only. Also, if the files is under version control, it will check the file out for you.                                                                         |

### Manipulate Windows

|  Key(s)   | Description                                                                                                                                                                                                                                                                              |
|:---------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   `C-v`   | `scroll-up`. The basic command to scroll forward (towards the end of the file) one screenful. By default Emacs leaves you two lines of context from the previous screen.                                                                                                                 |
|   `M-v`   | `scroll-down`. Just like `C-v`, but scrolls backwards.                                                                                                                                                                                                                                   |
| `C-x` `o` | `other-window`. Switch to another window, making it the active window. Repeated invocation of this command moves through all the windows, left to right and top to bottom, and then circles around again. Under a windowing system, you can use the left mouse button to switch windows. |
| `C-x` `1` | `delete-other-windows`. Deletes all other windows except the current one, making one window on the screen. Note that this in no way deletes the buffers or files associated with the deleted windows.                                                                                    |
| `C-x` `0` | `delete-window`. Deletes just the current window, resizing the others appropriately.                                                                                                                                                                                                     |
| `C-x` `2` | `split-window-vertically`. Splits the current window in two, vertically. This creates a new window, but not a new buffer: the same buffer will now be viewed in the two windows. This allows you to view two different parts of the same buffer simultaneously.                          |
| `C-x` `3` | `split-window-horizontally`. Splits the current window in two, horizontally. This creates a new window, but not a new buffer: the same buffer will now be viewed in the two windows. This allows you to view two different parts of the same buffer simultaneously.                      |
|  `C-M-v`  | `scroll-other-window`. Just like `C-v`, but scrolls the other window. If you have more than two windows, the other window is the window that `C-x` `o` would switch to.                                                                                                                  |

---

## References

- <http://www2.lib.uchicago.edu/keith/tcl-course/emacs-tutorial.html>
