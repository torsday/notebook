# SQLite

## Commandline

```sh
sqlite3 DB_FILE
```

```sql
.tables

.show	Displays current settings for various parameters
.databases	Provides database names and files
.quit	Quit sqlite3 program
.tables	Show current tables
.schema	Display schema of table
.header	Display or hide the output table header
.mode	Select mode for the output table
.dump	Dump database in SQL text format
```

### Timestamp for start of day yesterday

```sql
SELECT strftime('%s', datetime('now', 'start of day', '-1 day'));

SELECT strftime('%s', datetime('now', 'start of day'));
```

---

```sql
sqlite> SELECT datetime('now', 'start of day', '-1 day');
2017-07-13 00:00:00
sqlite> SELECT datetime('now', 'start of day');
2017-07-14 00:00:00
sqlite>

sqlite> SELECT strftime('%s', datetime('now', 'start of day', '-1 day'));
1499904000
sqlite> SELECT strftime('%s', datetime('now', 'start of day'));
1499990400


```

---

## Chrome History

"[Google Chrome's] timestamp is formatted as the number of microseconds since January, 1601"

~ https://stackoverflow.com/questions/20458406/what-is-the-format-of-chromes-timestamps

```sql
datetime(visit_time / 1000000 + (strftime('%s', '1601-01-01')), 'unixepoch')
```

```sqlite
SELECT * FROM urls WHERE last_visit_time BETWEEN strftime('%s', datetime('now', 'start of day', '-1 day')) AND strftime('%s', datetime('now', 'start of day'));
```

```sql
datetime(visit_time / 1000000 + (strftime('%s', '1601-01-01')), 'unixepoch')

SELECT (strftime('%s', datetime('now', 'start of day', '-1 day')) * 1000000) + strftime('%s', '1601-01-01'));
```


---

## Ruby Config

### `results_as_hash`

*From: [RubyDoc](http://www.rubydoc.info/github/luislavena/sqlite3-ruby/SQLite3%2FDatabase%3Aresults_as_hash)*

> A boolean that indicates whether rows in result sets should be returned as hashes or not. By default, rows are returned as arrays.

```ruby
@db = SQLite3::Database.new <path_to_db>

@db.results_as_hash = true
```

## Examples

### Get all URLS updated in the past 10 days

```sqlite
SELECT * FROM Urls WHERE UpdatedAt >= date('now','-10 day');
```

### Update value for rows updated in past 10 days

```sqlite
UPDATE Urls
SET Downloaded = 0
WHERE UpdatedAt >= date('now','-10 day');
```

### `GROUP BY`, `count()`, and `ORDER BY`

```sqlite
SELECT *, count(Url)
FROM Urls
GROUP BY HttpCode
ORDER BY count(Url) DESC;
```
