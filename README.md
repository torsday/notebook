# Notebook

A living document of my notes; largely engineering, but lifestyle as well. As a work-in-progress, they're not always clean and organized; this will improve as the project evolves. Check it out, let me know your thoughts.

-   Download the ePub here: <https://www.gitbook.com/download/epub/book/torsday/notebook>

![GitHub Avatar](https://avatars2.githubusercontent.com/u/281985?v=3&s=460)

-   [GitHub Link](https://github.com/torsday)
-   [ChrisTorstenson.com](http://www.christorstenson.com)
