# Markdown


---

## Links

```markdown
[Title](URL)
```

reference-style link

```markdown
[Title][id]

...

[id]: http://example.com/  "Optional Title Here"
```

---

## Tables

```markdown
Colons can be used to align columns.
There must be at least 3 dashes separating each header cell

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```

---

## Footnotes


---

## References

* [Daring Fireball](https://daringfireball.net/projects/markdown/syntax)
* [GitHub basics](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
* [GitHub cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#headers)
* [wordpress quick reference](https://en.support.wordpress.com/markdown-quick-reference/)
