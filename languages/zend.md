# Zend Frameworks

*From: [Wikipedia](https://en.wikipedia.org/wiki/Zend_Framework)*

> Sn open source, object-oriented web application framework implemented in PHP 5 and licensed under the [New BSD License](https://en.wikipedia.org/wiki/BSD_licenses#3-clause).
