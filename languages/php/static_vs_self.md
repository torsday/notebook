# Static vs Self in PHP

---

## Misc Notes

### you can't use static when defining the default value for a parameter

```php
class TestClass {
  const MY_CONST = MyOtherClass::CONST_IN_SAID_CLASS;

  protected function getMockCreditAlertDarwinService($version = static::MY_CONST)
  {}
}
```

results in

```
PHP Fatal error:  "static::" is not allowed in compile-time constants
```

whereas

```php
class TestClass {
  const MY_CONST = MyOtherClass::CONST_IN_SAID_CLASS;

  protected function getMockCreditAlertDarwinService($version = self::MY_CONST)
  {}
}
```

works fine.

---
