# Screen

*From: [Wikipedia](https://en.wikipedia.org/wiki/GNU_Screen)*

> a terminal multiplexer, a software application that can be used to multiplex several virtual consoles, allowing a user to access multiple separate login sessions inside a single terminal window, or detach and reattach sessions from a terminal. It is useful for dealing with multiple programs from a command line interface, and for separating programs from the session of the Unix shell that started the program, particularly so a remote process continues running even when the user is disconnected.

![](https://upload.wikimedia.org/wikipedia/commons/b/b5/Gnuscreen.png)

---

- [GNU Screen Survival Guide](https://stackoverflow.com/questions/70614/gnu-screen-survival-guide)
- [Screen vs TMux](https://superuser.com/questions/236158/tmux-vs-screen)
